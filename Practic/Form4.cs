﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul
{
    public partial class Form4 : Form
    {
        Button btn1 = new Button();
        public Form4()
        {
            InitializeComponent();
            btn1.Parent = this;
            btn1.Location = new Point(100, 30);
            btn1.Text = "Главная";
            btn1.Size = new Size(150, 20);
            btn1.Click += Btn1_Click;
            btn2.Click += Btn2_Click;
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText("E:\\TestFile.txt", tb1.Text.ToString());

            StreamReader sr = new StreamReader("E:\\TestFile.txt");
            string[] text = new string[3];
            for (int i = 0; i < 3; i++)
            {
                text[i] = sr.ReadLine();
            }
            sr.Close();
            for (int i = 2; i >= 0; i--)
            {
                tb2.Text=text[i];
            }
        }

        Form1 F1;
        private void Btn1_Click(object sender, EventArgs e)
        {
            F1 = new Form1();
            F1.Show();
            Hide();
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
