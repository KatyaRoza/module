﻿namespace Modul
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb1 = new System.Windows.Forms.Label();
            this.lb2 = new System.Windows.Forms.Label();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.lb3 = new System.Windows.Forms.Label();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.btn2 = new System.Windows.Forms.Button();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.Location = new System.Drawing.Point(222, 92);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(70, 13);
            this.lb1.TabIndex = 0;
            this.lb1.Text = "Задание №3";
            // 
            // lb2
            // 
            this.lb2.AutoSize = true;
            this.lb2.Location = new System.Drawing.Point(85, 134);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(27, 13);
            this.lb2.TabIndex = 1;
            this.lb2.Text = "R = ";
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(118, 131);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(174, 20);
            this.tb1.TabIndex = 2;
            // 
            // lb3
            // 
            this.lb3.AutoSize = true;
            this.lb3.Location = new System.Drawing.Point(85, 179);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(26, 13);
            this.lb3.TabIndex = 3;
            this.lb3.Text = "X = ";
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(118, 176);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(174, 20);
            this.tb2.TabIndex = 4;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(171, 235);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(121, 31);
            this.btn2.TabIndex = 5;
            this.btn2.Text = "Вычислить";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // tb3
            // 
            this.tb3.Location = new System.Drawing.Point(48, 290);
            this.tb3.Multiline = true;
            this.tb3.Name = "tb3";
            this.tb3.Size = new System.Drawing.Size(361, 148);
            this.tb3.TabIndex = 6;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 450);
            this.Controls.Add(this.tb3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.lb3);
            this.Controls.Add(this.tb1);
            this.Controls.Add(this.lb2);
            this.Controls.Add(this.lb1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb1;
        private System.Windows.Forms.Label lb2;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.Label lb3;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.TextBox tb3;
    }
}