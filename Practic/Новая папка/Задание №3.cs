﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul
{
    public partial class Form2 : Form
    {
        Button btn1 = new Button();
        public Form2()
        {
            InitializeComponent();
            btn1.Parent = this;
            btn1.Location = new Point(100, 30);
            btn1.Text = "Главная";
            btn1.Size = new Size(150, 20);
            btn1.Click += Btn1_Click;
        }


        Form1 F1;
        private void Btn1_Click(object sender, EventArgs e)
        {
            F1 = new Form1();
            F1.Show();
            Hide();
        }
    }
}
