﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul
{
    public partial class Form1 : Form
    {
        Button btn1 = new Button();
        Button btn2 = new Button();
        Button btn3 = new Button();

        public Form1()
        {
            InitializeComponent();
            btn1.Parent = this;
            btn1.Location = new Point(100,40);
            btn1.Text = "Задание №3";
            btn1.Size= new Size(150, 20);
            btn1.Click += Btn1_Click;

            btn2.Parent = this;
            btn2.Location = new Point(100, 100);
            btn2.Text = "Задание №6";
            btn2.Size = new Size(150, 20);
            btn2.Click += Btn2_Click;

            btn3.Parent = this;
            btn3.Location = new Point(100, 120);
            btn3.Text = "Задание №7";
            btn3.Size = new Size(150, 20);
            btn3.Click += Btn3_Click;


        }
        Form4 F4;
        private void Btn3_Click(object sender, EventArgs e)
        {
            F4 = new Form4();
            F4.Show();
            Hide();
        }

        Form3 F3;
        private void Btn2_Click(object sender, EventArgs e)
        {
            F3 = new Form3();
            F3.Show();
            Hide();
        }

        Form2 F2;
        private void Btn1_Click(object sender, EventArgs e)
        {
            F2 = new Form2();
            F2.Show();
            Hide();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
