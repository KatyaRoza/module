﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul
{
    public partial class Form2 : Form
    {
        Button btn1 = new Button();
        public Form2()
        {
            InitializeComponent();
            btn1.Parent = this;
            btn1.Location = new Point(100, 30);
            btn1.Text = "Главная";
            btn1.Size = new Size(150, 20);
            btn1.Click += Btn1_Click;
            btn2.Click += Btn2_Click;
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            try
            {
                double Y = 0;
                double X = Convert.ToDouble(tb2.Text);

                if (-9 <= X && X < -6)
                {
                    double R = Convert.ToDouble(tb1.Text);
                    Y = -Math.Sqrt(Math.Pow(R, 2) - Math.Pow((X + 6), 2));
                }
                else if (-6 <= X && X <= -3)
                {
                    Y = X + 3;
                }
                else if (-3 < X && X < 0)
                {
                    double R = Convert.ToDouble(tb1.Text);
                    Y = Math.Sqrt(Math.Pow(R, 2) - Math.Pow(X, 2));
                }
                else if (0 <= X && X <= 3)
                {
                    Y = -X + 3;
                }
                else 
                {
                    Y = 0.5 * X - 1.5;
                }

                tb3.Text = "Значение ординаты: " + Y;
            }
            catch {
                tb3.Text = "Ошибка вычисления";
            }
        }

        Form1 F1;
        private void Btn1_Click(object sender, EventArgs e)
        {
            F1 = new Form1();
            F1.Show();
            Hide();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
