﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Modul
{
    public partial class Form3 : Form
    {
        Button btn1 = new Button();
        public Form3()
        {
            InitializeComponent();
            btn1.Parent = this;
            btn1.Location = new Point(100, 30);
            btn1.Text = "Главная";
            btn1.Size = new Size(150, 20);
            btn1.Click += Btn1_Click;
            btn2.Click += Btn2_Click;
            
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            try
            {
                int n = Convert.ToInt32(tb1.Text);
                int[] array = new int[n];
            Random rand = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(-20,20);
                tb3.Text = tb3.Text + " " + array[i] + " ";
            }

                int Sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    Sum += array[i];
                }
            }
            lb1.Items.Add("Сумма отрицательных элементов массива: "+ Sum);

            int max = array[0], min = array[0];
            int imax=0, imin=0;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] >= max) { max = array[i]; imax = i; }
                    if (array[i] <= min) { min = array[i]; imin = i; }
                }
                double value = 1;
                for (int i = 0; i < array.Length; i++)
                {
                    if (imin < imax)
                    {
                        if (i > imin && i < imax)
                        {
                            value *= array[i];
                        }
                    }
                    else if (imax < imin)
                    {
                        if (i < imin && i > imax)
                        {
                            value *= array[i];
                        }
                    }
                }
                lb1.Items.Add("Произведение: " + value);

                Array.Sort(array);
                for (int i = 0; i < array.Length; i++)
                {
                    tb2.Text = tb2.Text + " " + array[i] + " ";
                }
            }
            catch
            {
                MessageBox.Show("Введите значение N", "Ошибка");
            }

        }

        Form1 F1;
        private void Btn1_Click(object sender, EventArgs e)
        {
            F1 = new Form1();
            F1.Show();
            Hide();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
